PHP Sandbox
===========

This is a cleaned and improved continuation of [Seldaek/php-console](https://github.com/Seldaek/php-console), 
forked in 2015, and as of 2018 turned into a Laravel package and published to packagist as `mightypork/php-sandbox`.

PHP Sandbox is a web editor to try your PHP code. It allows you to run small bits of code in the context of your 
Laravel application, e.g. to test functionality or verify algorithms.

There is no isolation or security built in, it is literally `eval()` with user-provided code. 

Absolutely DO NOT run this on a production server, and if you must, ensure it is not publicly accessible. 
PHP Sandbox is meant for a local dev environment. It is disabled by default, and you can enable it using `.env` 
(see the Installation section below).


Screenshot
----------

![screenshot](screenshot.png)

**Tip:** Press Ctrl+Enter to evaluate the code


Installation
------------

It is a Composer package for the Laravel framework.

- Install it (`composer require mightypork/php-sandbox`), then `composer dump-autoload`.
- The service provider should be automatically found by Laravel via the discover functionality.
  If it is not, add the service provider to your `config/app.php` file (providers section): `MightyPork\PhpSandbox\PhpSandboxServiceProvider::class`
- Run `php artisan vendor:publish` and select the package; this copies the JS and CSS assets to your public folder, and creates the config file (`config/php-sandbox.php`).
- Add `ENABLE_PHP_SANDBOX=true` to your .env file to enable access

The sandbox is available at `/sandbox`. 

If you wish to change the path, that can be done in the config file. You can also decide which middleware to use.


About this fork
---------------

This is a fork of the original code by Seldaek (Jordi Boggiano), as it was on GitHub in 2015, cleaned and turned into a Laravel package.

**Changes:**

- Changed colors to look more like Sublime Text, with Monokai theme
- Slightly improved layout
- Removed info text at the bottom of the screen
- Output is now plain text, not HTML (more useful for debugging). However, some html output works too (in particular `dd()`)
- Removed "krumo", "clippy", and "Melody" (bloat from the original version)
- Updated ACE to 1.2.0 with a patch to support the PHP trait keyword
- Removed the loader animation (it was broken)


License
-------

PHP Sandbox is licensed under the New BSD License.


Why is this not on GitHub
-------------------------

I don't like Microsoft.
