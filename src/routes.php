<?php

// sandbox
if (config('php-sandbox.enable', false)) {
    Route::group([
        'middleware' => config('php-sandbox.route_middleware'),
    ], function () {
        Route::get(config('php-sandbox.route', 'sandbox'),
            '\MightyPork\PhpSandbox\PhpSandboxController@index')->name('php-sandbox');

        Route::post(config('php-sandbox.route', 'sandbox'),
            '\MightyPork\PhpSandbox\PhpSandboxController@run'); // ajax
    });
}
