<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="_token" content="{{ csrf_token() }}">

	<title>PHP debug sandbox</title>

	<link rel="stylesheet" type="text/css" href="/vendor/php-sandbox/styles.css"/>

	<script src="/vendor/php-sandbox/jquery-1.9.1.min.js"></script>
	<script src="/vendor/php-sandbox/ace/ace.js"></script>
	<script src="/vendor/php-sandbox/ace/theme-monokai.js"></script>
	<script src="/vendor/php-sandbox/ace/mode-php.js"></script>
	<script src="/vendor/php-sandbox/moment.min.js"></script>
	<script src="/vendor/php-sandbox/ace/ext-language_tools.js"></script>
	<script src="/vendor/php-sandbox/php-console.js"></script>

	<script>
		var defcode = @json($defcode);

		$.console({
			tabsize: 4
		});
	</script>
</head>
<body>
<div class="console-wrapper">
	<div class="input-wrapper">
		<form method="POST" action="" id="mainform">
			<div class="input">
				<textarea class="editor" id="editor" name="code">{{ $code or "&lt;?php\n" }}</textarea>
			</div>
			<div class="statusbar">
				<select id="saves"></select>&nbsp;
				<a id="btn-new">new</a>
				<a id="btn-ren">ren</a>
				<a id="btn-del">del</a>
				&nbsp;|&nbsp;
				<span class="position">@ 1:1</span>
				<span class="runtime-info"></span>
				<input type="submit" name="subm" value="Run!"/>
			</div>
		</form>
	</div>
	<div class="output-wrapper">
		<div class="output">{{ $output or '' }}</div>
	</div>
</div>
</body>
</html>
