<?php

namespace MightyPork\PhpSandbox;

use Illuminate\Http\Request;
use Response;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Development sandbox.
 */
class PhpSandboxController extends \Illuminate\Routing\Controller
{
    use ValidatesRequests;

	public function index()
	{
		return view('php-sandbox::sandbox', [
			'defcode' => $this->getDefcode()
		]);
	}

	private function evaluateCode($code)
	{
		ini_set('display_errors', true);
		ini_set('display_startup_errors', true);
		ini_set('display_not_found_reason', true);
		ini_set('display_exceptions', true);
		ini_set('html_errors', false);

		error_reporting(E_ALL | E_STRICT);

		ob_start();
		$memBefore = memory_get_usage(true);
		$startTime = microtime(true);

		// Remove the <?php mark
		$code = preg_replace('{^\s*<\?(php)?\s*}i', '', $code);

		/** Run code with bootstrap in separate scope */
		function runCode($__source_code)
		{
			eval($__source_code);
		}

		try {
			runCode($code);
		} catch (\Exception $e) {
			// dump & die - shows nice expandable view of the exception.
			dd($e);
		}

		$endTime = microtime(true);
		$memAfter = memory_get_peak_usage(true);

		$output = ob_get_clean();

		$memory = ($memAfter - $memBefore) / 1024.0 / 1024.0; // in MB
		$duration = ($endTime - $startTime) * 1000; // in ms

		return compact('memory', 'duration', 'output');
	}

	public function run(Request $request)
	{
		$this->validate($request, [
			'code' => 'required'
		]);

		$out = $this->evaluateCode($request->code);

		if ($request->wantsJson()) {

			$resp = Response::json([
				'output' => $out['output'] . "\n#end-php-console-output#",
				'memory' => sprintf('%.3f', $out['memory']),
				'duration' => sprintf('%.3f', $out['duration'])
			], 200);

			return $resp;

		} else {

			// Full HTML page
			return view('php-sandbox::sandbox', [
				'code' => $request->code,
				'defcode' => $this->getDefcode(),
				'output' => $out['output']
			]);

		}
	}

	private function getDefcode()
	{
		return config('php-sandbox.default_code');
	}
}
