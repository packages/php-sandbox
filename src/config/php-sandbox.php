<?php

return [
    'enable' => env('ENABLE_PHP_SANDBOX', false),

    'route' => 'sandbox',

    'route_middleware' => ['web'],

    'default_code' => <<<CODE
<?php


CODE
];
