<?php

namespace MightyPork\PhpSandbox;

use Illuminate\Support\ServiceProvider;

class PhpSandboxServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';

        $this->publishes([
            __DIR__.'/assets' => public_path('vendor/php-sandbox'),
        ], 'public');

        $this->publishes([
            __DIR__.'/config/php-sandbox.php' => config_path('php-sandbox.php')
        ], 'config');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(PhpSandboxController::class);
        $this->loadViewsFrom(__DIR__.'/views', 'php-sandbox');
    }
}
